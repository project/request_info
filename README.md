# Request info
initially developed by drunomics GmbH <hello@drunomics.com>

Adds request information based on Drupal's status report. This is helpful to verify that Drupal is configured correctly;
e.g., trusted reverse proxy addresses are configured correctly and HTTP headers are followed.

## Usage

* Install as usual.
* Log in as admin, open status report at "admin/reports/status". 
